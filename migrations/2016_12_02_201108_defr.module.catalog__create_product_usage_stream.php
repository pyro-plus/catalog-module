<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleCatalogCreateProductUsageStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'product_usages',
        'title_column' => 'name',
        'translatable' => true,
        'trashable'    => true,
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'             => [
            'translatable' => true,
            'required'     => true,
            'unique'       => true,
        ],
        'slug'             => [
            'required' => true,
            'unique'   => true,
            'config'   => [
                'slugify' => 'name',
            ],
        ],
        'description'      => [
            'translatable' => true,
        ],
        'content'          => [
            'translatable' => true,
        ],
        'image',
        'preview',
        'video',
        'meta_title'       => [
            'translatable' => true,
        ],
        'meta_description' => [
            'translatable' => true,
        ],
        'meta_keywords'    => [
            'translatable' => true,
        ],
    ];

}
