<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleCatalogCreateVariantsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'variants',
        'title_column' => 'sku',
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        // 'product' => [
        //     'required' => true,
        // ],
        'sku'           => [
            'unique'   => true,
            'required' => true,
        ],
        'color',
        'value' => [
            'config' => [
                'label' => 'Тара',
            ],
        ],
        'capacity',
        'price',
    ];

}
