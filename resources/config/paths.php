<?php

use Defr\CatalogModule\Product\Contract\ProductInterface;

return [

    /*
    |--------------------------------------------------------------------------
    | Module
    |--------------------------------------------------------------------------
    |
    | Specify the front end module path.
    |
    | i.e. 'catalog' yields http://yoursite.com/catalog
    |
     */

    'module'    => 'catalog',

    /*
    |--------------------------------------------------------------------------
    | Category
    |--------------------------------------------------------------------------
    |
    | Specify the front end category path.
    |
    | i.e. 'category' yields http://yoursite.com/catalog/category/{category}
    |
     */

    'category'  => 'category',

    /*
    |--------------------------------------------------------------------------
    | Brand
    |--------------------------------------------------------------------------
    |
    | Specify the front end brand path.
    |
    | i.e. 'brand' yields http://yoursite.com/catalog/brand/{brand}
    |
     */

    'brand'     => 'brand',

    /*
    |--------------------------------------------------------------------------
    | ProductType
    |--------------------------------------------------------------------------
    |
    | Specify the front end type path.
    |
    | i.e. 'type' yields http://yoursite.com/catalog/type/{type}
    |
     */

    'type'      => 'type',

    /*
    |--------------------------------------------------------------------------
    | Tags
    |--------------------------------------------------------------------------
    |
    | Specify the front end tag path.
    |
    | i.e. 'category' yields http://yoursite.com/catalog/tag/{tag}
    |
     */

    'tag'       => 'tag',

    /*
    |--------------------------------------------------------------------------
    | Permalink
    |--------------------------------------------------------------------------
    |
    | Specify the product permalink pattern. This value supports handlers,
    | closures, and value strings.
    |
     */

    'permalink' => function (ProductInterface $product)
    {
        return 'product/'.$product->getSlug();
    },

    /*
    |--------------------------------------------------------------------------
    | Route
    |--------------------------------------------------------------------------
    |
    | The route is simple the route pattern equivalent to the permalink
    | you specify. The route MUST have '{product}' present in it.
    |
     */

    'route'     => '{year}/{month}/{day}/{product}',

];
