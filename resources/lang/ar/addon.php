<?php

return [
    'title'       => 'المنشورات',
    'name'        => 'وحدة المنشورات',
    'description' => 'ادارة المنشورات والمقالات المنوعة.',
    'section'     => [
        'products'      => 'المنشورات',
        'types'      => 'الأنواع',
        'fields'     => 'الحقول',
        'categories' => 'الأصناف',
    ],
];
