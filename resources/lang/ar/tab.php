<?php

return [
    'organization' => 'المؤسسة',
    'permalinks'   => 'الروابط الثابته',
    'advanced'     => 'متقدم',
    'general'      => 'عام',
    'display'      => 'عرض',
    'options'      => 'خيارت',
    'fields'       => 'حقول',
    'layout'       => 'الهيكلية',
    'product'         => 'منشور',
    'seo'          => 'محركات البحث',
];
