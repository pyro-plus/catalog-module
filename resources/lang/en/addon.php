<?php

return [
    'title'       => 'Catalog',
    'name'        => 'Catalog Module',
    'description' => '',
    'section'     => [
        'products'   => 'Products',
        'categories' => 'Categories',
        'types'      => 'ProductTypes',
        'brands'     => 'Brands',
        'properties' => 'Properties',
        'colors'     => 'Colors',
    ],
];
