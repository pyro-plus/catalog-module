<?php

return [
    'products' => 'Products',
    'fields'   => 'Fields',
    'archive'  => 'Archive',
    'tagged'   => 'Tagged ":tag"',
];
