<?php

return [
    'new_product'  => 'Add Product',
    'new_category' => 'Add Category',
    'new_type'     => 'Add ProductType',
    'new_property' => 'Add Property',
    'new_color'    => 'Add Color',
    'new_brand'    => 'Add Brand',
    'view'         => 'View on site',
    'send_message' => 'Send message',
];
