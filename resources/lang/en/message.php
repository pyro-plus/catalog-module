<?php

return [
    'choose_type'       => 'What type of product would you like to create?',
    'choose_field_type' => 'What field type would you like to use?',
    'choose_field'      => 'What field would you like to use?',
    'tagged'            => 'Tagged ":tag"',
    'scheduled'         => 'Scheduled',
    'status'            => 'Status',
    'live'              => 'Live',
    'draft'             => 'Draft',
];
