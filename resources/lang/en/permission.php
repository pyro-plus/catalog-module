<?php

return [
    'products'   => [
        'name'   => 'Products',
        'option' => [
            'read'   => 'Can access products section.',
            'write'  => 'Can create and edit products.',
            'delete' => 'Can delete products.',
        ],
    ],
    'categories' => [
        'name'   => 'Categories',
        'option' => [
            'read'   => 'Can access categories section.',
            'write'  => 'Can create and edit categories.',
            'delete' => 'Can delete categories.',
        ],
    ],
    'types'      => [
        'name'   => 'ProductTypes',
        'option' => [
            'read'   => 'Can access types section.',
            'write'  => 'Can create and edit types.',
            'delete' => 'Can delete types.',
        ],
    ],
    'fields'     => [
        'name'   => 'Fields',
        'option' => [
            'manage' => 'Can manage custom fields.',
        ],
    ],
];
