<?php

return [
    'products'      => [
        'name' => 'Products',
    ],
    'product_types' => [
        'name' => 'Product ProductTypes',
    ],
    'product_usages'    => [
        'name' => 'Product Usages',
    ],
];
