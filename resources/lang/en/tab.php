<?php

return [
    'organization' => 'Organization',
    'permalinks'   => 'Permalinks',
    'advanced'     => 'Advanced',
    'general'      => 'General',
    'display'      => 'Display',
    'options'      => 'Options',
    'fields'       => 'Fields',
    'layout'       => 'Layout',
    'product'      => 'Product',
    'seo'          => 'SEO',
];
