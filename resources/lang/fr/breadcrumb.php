<?php

return [
    'products'   => 'Articles',
    'fields'  => 'Champs',
    'archive' => 'Archive',
    'tagged'  => 'Taggés ":tag"',
];
