<?php

return [
    'title'       => 'Hírek',
    'name'        => 'Hírek modul',
    'description' => 'Komplex hír és blog kezelő',
    'section'     => [
        'products'  => 'Hírek',
    'types'      => 'Típusok',
    'fields'     => 'Mezők',
    'categories' => 'Kategóriák',
    ],
];
