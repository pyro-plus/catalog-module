<?php

return [
    'title'       => 'Каталог',
    'name'        => 'Модуль Каталог',
    'description' => 'Управление товарами',
    'section'     => [
        'products'       => 'Товары',
        'product_usages' => 'Области применения',
        'types'          => 'Типы товаров',
        'brands'         => 'Бренды',
        'properties'     => 'Характеристики',
    ],
];
