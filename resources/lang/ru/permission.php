<?php

return [
    'products'   => [
        'name'   => 'Товары',
        'option' => [
            'read'   => 'Can access products section.',
            'write'  => 'Can create and edit products.',
            'delete' => 'Can delete products.',
        ],
    ],
    'categories' => [
        'name'   => 'Области применения',
        'option' => [
            'read'   => 'Can access categories section.',
            'write'  => 'Can create and edit categories.',
            'delete' => 'Can delete categories.',
        ],
    ],
    'types'      => [
        'name'   => 'Типы товаров',
        'option' => [
            'read'   => 'Can access types section.',
            'write'  => 'Can create and edit types.',
            'delete' => 'Can delete types.',
        ],
    ],
    'fields'     => [
        'name'   => 'Характеристики',
        'option' => [
            'manage' => 'Can manage custom fields.',
        ],
    ],
];
