<?php

return [
    'products'      => [
        'name' => 'Товары',
        'description' => 'Товары',
    ],
    'types' => [
        'name' => 'Типы товаров',
        'description' => 'Типы товаров',
    ],
    'product_usages'    => [
        'name' => 'Области применения',
        'description' => 'Области применения',
    ],
    'brands'    => [
        'name' => 'Бренды',
        'description' => 'Бренды',
    ],
    'variants'    => [
        'name' => 'Вариации',
        'description' => 'Различные вариации товара',
    ],
    'properties'    => [
        'name' => 'Физические свойства',
        'description' => 'Таблица физических свойств товара',
    ],
];
