<?php

return [
    'title'       => 'Inlägg',
    'name'        => 'Inläggsmodul',
    'description' => 'En mångsidig modul som sköter artiklar och inlägg.',
    'section'     => [
        'products'      => 'Inlägg',
        'types'      => 'ProductTyper',
        'fields'     => 'Fält',
        'settings'   => 'Inställningar',
        'categories' => 'Kategorier',
    ],
];
