<?php

return [
    'products'  => 'Products',
    'fields' => 'Fields',
    'tagged' => 'Tagged ":tag"',
];
