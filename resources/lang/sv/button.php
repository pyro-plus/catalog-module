<?php

return [
    'new_product'     => 'Nytt Inlägg',
    'new_category' => 'Ny Kategori',
    'new_type'     => 'Ny Typ',
    'new_field'    => 'Nytt Fält',
    'add_field'    => 'Lägg Till Fält',
];
