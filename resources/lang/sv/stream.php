<?php

return [
    'products'      => [
        'name' => 'Inlägg',
    ],
    'product_types' => [
        'name' => 'Inläggstyper',
    ],
    'categories' => [
        'name' => 'Kategorier',
    ],
];
