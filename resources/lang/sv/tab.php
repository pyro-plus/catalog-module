<?php

return [
    'organization' => 'Organisation',
    'permalinks'   => 'Permalänk',
    'advanced'     => 'Avancerat',
    'general'      => 'Allmänt',
    'display'      => 'Display',
    'options'      => 'Alternativ',
    'fields'       => 'Fält',
    'layout'       => 'Layout',
    'product'         => 'Inlägg',
    'seo'          => 'SEO',
    'css'          => 'CSS',
    'js'           => 'JS',
];
