<?php namespace Defr\CatalogModule\Brand;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\CatalogModule\Brand\Contract\BrandRepositoryInterface;

class BrandRepository extends EntryRepository implements BrandRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var BrandModel
     */
    protected $model;

    /**
     * Create a new BrandRepository instance.
     *
     * @param BrandModel $model
     */
    public function __construct(BrandModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find a brand by it's slug.
     *
     * @param  $slug
     * @return null|BrandInterface
     */
    public function findBySlug($slug)
    {
        return $this->model->orderBy('created_at', 'DESC')
        ->where('slug', $slug)->first();
    }

    /**
     * Makes array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection()
    {
        return $this->model->all()
        /* @var BrandModel $brand */
        ->map(function ($brand)
        {
            return [
                'id'          => $brand->getId(),
                'name'        => $brand->getName(),
                'slug'        => $brand->getSlug(),
                'image_url'   => $brand->getImageUrl(),
                'button_text' => $brand->getButtonText(),
                'description' => $brand->getDescription(),
            ];
        });
    }
}
