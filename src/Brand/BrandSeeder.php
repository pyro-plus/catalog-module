<?php namespace Defr\CatalogModule\Brand;

use Illuminate\Contracts\Config\Repository;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\CatalogModule\Brand\Contract\BrandRepositoryInterface;

/**
 * Class BrandSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class BrandSeeder extends Seeder
{
    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * The brands repository.
     *
     * @var Repository
     */
    protected $brands;

    /**
     * Create a new BrandSeeder instance.
     *
     * @param Repository $config
     */
    public function __construct(
        Repository $config,
        BrandRepositoryInterface $brands
    )
    {
        $this->config = $config;
        $this->brands = $brands;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        echo "\n\n\033[34;5;228mStarting brands seeder!\n";

        $this->brands->truncate();

        echo "\033[35;5;228mBrands cleared!\n";

        if ($data = file_get_contents(__DIR__.'/../../resources/seeders/brands.json'))
        {
            $trans = json_decode(file_get_contents(__DIR__.'/../../resources/seeders/brands_trans.json'), true);

            foreach (json_decode($data, true) as $brand)
            {
                // $brandProperties  = [];
                // $brandName        = array_get($brand, 'name');
                // $brandDescription = array_get($brand, 'description');
                $name = '';

                foreach (array_filter(
                    $trans,
                    function ($transEntry) use ($brand, $name)
                    {
                        $name = array_get($transEntry, 'name', '');
                        return array_get($transEntry, 'entry_id') === array_get($brand, 'id');
                    }
                ) as $translation)
                {
                    array_set($brand, array_get($translation, 'locale'), $translation);
                }

                /* @var ProductBrandInterface $brand */
                $brand = $this->brands->create($brand);

                echo "\033[36;5;228mCreated brand \033[31;5;228m".$name."\n";
            }

            echo "\033[32;5;228mBrands seeded successfully!\n";
        }
    }
}
