<?php namespace Defr\CatalogModule;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\CatalogModule\Brand\BrandSeeder;
use Defr\CatalogModule\Color\ColorSeeder;
use Defr\CatalogModule\ProductType\ProductTypeSeeder;
use Defr\CatalogModule\ProductUsage\ProductUsageSeeder;
use Defr\CatalogModule\Product\ProductSeeder;

class CatalogModuleSeeder extends Seeder
{

    /**
     * Run the seeder
     */
    public function run()
    {
        $this->call(BrandSeeder::class);
        $this->call(ProductUsageSeeder::class);
        $this->call(ProductTypeSeeder::class);
        $this->call(ProductSeeder::class);
    }
}
