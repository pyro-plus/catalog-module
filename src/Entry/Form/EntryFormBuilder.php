<?php namespace Defr\CatalogModule\Entry\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

/**
 * Class EntryFormBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class EntryFormBuilder extends FormBuilder
{

}
