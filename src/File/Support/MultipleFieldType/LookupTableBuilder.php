<?php namespace Defr\CatalogModule\File\Support\MultipleFieldType;

use Anomaly\FileFieldType\Table\FileTableFilters;

/**
 * Class LookupTableBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 *
 * @link          http://pyrocms.com/
 */
class LookupTableBuilder extends \Anomaly\FilesFieldType\Table\FileTableBuilder
{

    /**
     * The table columns.
     *
     * @var array
     */
    protected $columns = [
        'entry.preview' => [
            'heading' => 'anomaly.module.files::field.preview.name',
        ],
        'name'          => [
            'sort_column' => 'name',
            'wrapper'     => '
                    <strong>{value.file}</strong>
                    <br>
                    <small class="text-muted">{value.disk}://{value.folder}/{value.file}</small>
                    <br>
                    <span>{value.size} {value.keywords}</span>',
            'value'       => [
                'file'     => 'entry.name',
                'folder'   => 'entry.folder.slug',
                'keywords' => 'entry.keywords.labels|join',
                'disk'     => 'entry.folder.disk.slug',
                'size'     => 'entry.size_label',
                'podpis'   => 'entry.podpis_ikonki',
            ],
        ],
        'size'          => [
            'sort_column' => 'size',
            'value'       => 'entry.readable_size',
        ],
        'mime_type',
        'folder',
        'podpis_ikonki' => [
            'heading' => 'defr.module.catalog::field.podpis_ikonki.name',
            'wrapper' => '<span>{value.podpis}</span>',
            'value'   => [
                'podpis' => 'entry.podpis_ikonki',
            ],
        ],
    ];
}
