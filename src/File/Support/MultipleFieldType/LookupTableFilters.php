<?php namespace Defr\CatalogModule\File\Support\MultipleFieldType;

/**
 * Class LookupTableFilters
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Ryan Thompson <ryan@pyrocms.com>
 *
 * @link          http://pyrocms.com/
 */
class LookupTableFilters extends \Anomaly\FilesFieldType\Table\FileTableFilters
{

}
