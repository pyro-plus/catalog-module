<?php namespace Defr\CatalogModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Assignment\Form\AssignmentFormBuilder;
use Anomaly\Streams\Platform\Assignment\Table\AssignmentTableBuilder;
use Anomaly\Streams\Platform\Field\Contract\FieldRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Defr\CatalogModule\ProductType\Form\ProductTypeFormBuilder;
use Defr\CatalogModule\ProductType\Table\ProductTypeTableBuilder;

/**
 * Class ProductTypesController
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductTypesController extends AdminController
{

    /**
     * Return an index of types.
     *
     * @param  ProductTypeTableBuilder $table
     * @return Response
     */
    public function index(ProductTypeTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Return the form for creating a new type.
     *
     * @param  ProductTypeFormBuilder $form
     * @return Response
     */
    public function create(ProductTypeFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Return the form for editing an existing type.
     *
     * @param  ProductTypeFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(ProductTypeFormBuilder $form, $id)
    {
        return $form->render($id);
    }

    /**
     * Return a table of existing product type assignments.
     *
     * @param  ProductTypeRepositoryInterface $types
     * @param  AssignmentTableBuilder         $table
     * @param  BreadcrumbCollection           $breadcrumbs
     * @param  $id
     * @return Response
     */
    public function properties(
        ProductTypeRepositoryInterface $types,
        AssignmentTableBuilder $table,
        BreadcrumbCollection $breadcrumbs,
        $id
    )
    {
        $type = $types->find($id);

        $breadcrumbs->put(
            $type->getTitle(),
            'admin/catalog/types/edit/'.$id
        );
        $breadcrumbs->put(
            'streams::breadcrumb.assignments',
            'admin/catalog/types/assignments/'.$id
        );

        return $table
        ->setButtons(
            [
                'edit' => [
                    'href' => 'admin/catalog/types/assignments/'.$id.'/assignment/{entry.id}',
                ],
            ]
        )
        ->setStream($type->getEntryStream())
        ->render();
    }

    /**
     * Return the modal for choosing a field to assign.
     *
     * @param  FieldRepositoryInterface $fields
     * @return \Illuminate\View\View
     */
    public function choose(
        FieldRepositoryInterface $fields,
        ProductTypeRepositoryInterface $types,
        $id
    )
    {
        $type = $types->find($id);

        return view(
            'module::admin/ajax/choose_field',
            [
                'fields' => $fields->findAllByNamespace('catalog')
                ->notAssignedTo($type->getEntryStream())->unlocked(),
                'id'     => $id,
            ]
        );
    }

    /**
     * Assign a field to a product type.
     *
     * @param  ProductTypeRepositoryInterface $types
     * @param  FieldRepositoryInterface       $fields
     * @param  AssignmentFormBuilder          $form
     * @param  $id
     * @param  $field
     * @return Response
     */
    public function assign(
        ProductTypeRepositoryInterface $types,
        FieldRepositoryInterface $fields,
        AssignmentFormBuilder $form,
        $id,
        $field
    )
    {
        $type = $types->find($id);

        return $form
        ->setStream($type->getEntryStream())
        ->setField($fields->find($field))
        ->render();
    }

    /**
     * Return a form for an existing product type field and assignment.
     *
     * @param  ProductTypeRepositoryInterface $types
     * @param  BreadcrumbCollection           $breadcrumbs
     * @param  AssignmentFormBuilder          $form
     * @param  $id
     * @return Response
     */
    public function assignment(
        ProductTypeRepositoryInterface $types,
        BreadcrumbCollection $breadcrumbs,
        AssignmentFormBuilder $form,
        $id,
        $assignment
    )
    {
        $breadcrumbs->put(
            'streams::breadcrumb.assignments',
            'admin/catalog/types/assignments/'.$id
        );

        return $form->render($assignment);
    }
}
