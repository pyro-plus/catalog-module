<?php namespace Defr\CatalogModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\CatalogModule\ProductUsage\Form\ProductUsageFormBuilder;
use Defr\CatalogModule\ProductUsage\Table\ProductUsageTableBuilder;

class ProductUsagesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  ProductUsageTableBuilder $table
     * @return Response
     */
    public function index(ProductUsageTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  ProductUsageFormBuilder $form
     * @return Response
     */
    public function create(ProductUsageFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  ProductUsageFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(ProductUsageFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
