<?php namespace Defr\CatalogModule\Http\Controller\Admin;

use Defr\CatalogModule\Property\Form\PropertyFormBuilder;
use Defr\CatalogModule\Property\Table\PropertyTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class PropertiesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param PropertyTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(PropertyTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param PropertyFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(PropertyFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param PropertyFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(PropertyFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
