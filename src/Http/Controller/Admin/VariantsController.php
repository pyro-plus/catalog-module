<?php namespace Defr\CatalogModule\Http\Controller\Admin;

use Defr\CatalogModule\Variant\Form\VariantFormBuilder;
use Defr\CatalogModule\Variant\Table\VariantTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class VariantsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param VariantTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(VariantTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param VariantFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(VariantFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param VariantFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(VariantFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
