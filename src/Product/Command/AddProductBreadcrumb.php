<?php namespace Defr\CatalogModule\Product\Command;

use Illuminate\Http\Request;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;

/**
 * Class AddProductBreadcrumb
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductBreadcrumb
{

    /**
     * The product instance.
     *
     * @var ProductInterface
     */
    protected $product;

    /**
     * Create a new AddProductBreadcrumb instance.
     *
     * @param ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Handle the command.
     *
     * @param Request              $request
     * @param BreadcrumbCollection $breadcrumbs
     */
    public function handle(Request $request, BreadcrumbCollection $breadcrumbs)
    {
        $breadcrumbs->add(
            $this->product->getTitle(),
            $request->fullUrl()
        );
    }
}
