<?php namespace Defr\CatalogModule\Product\Command;

use Illuminate\Contracts\Config\Repository;
use Anomaly\Streams\Platform\Ui\Breadcrumb\BreadcrumbCollection;

/**
 * Class AddProductsBreadcrumb
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductsBreadcrumb
{

    /**
     * Handle the command.
     *
     * @param Repository           $config
     * @param BreadcrumbCollection $breadcrumbs
     */
    public function handle(Repository $config, BreadcrumbCollection $breadcrumbs)
    {
        $breadcrumbs->add(
            'defr.module.catalog::breadcrumb.products',
            $config->get('defr.module.catalog::paths.module')
        );
    }
}
