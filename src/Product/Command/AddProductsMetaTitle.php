<?php namespace Defr\CatalogModule\Product\Command;

use Anomaly\Streams\Platform\View\ViewTemplate;

/**
 * Class AddProductsMetaTitle
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 */
class AddProductsMetaTitle
{

    /**
     * Set the meta name.
     *
     * @param ViewTemplate $template
     */
    public function handle(ViewTemplate $template)
    {
        $template->set('meta_title', trans('defr.module.catalog::breadcrumb.products'));
    }
}
