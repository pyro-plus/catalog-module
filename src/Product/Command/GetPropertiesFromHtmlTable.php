<?php namespace Defr\CatalogModule\Product\Command;

/**
 * Class for get properties from html table.
 */
class GetPropertiesFromHtmlTable
{

    /**
     * Html string
     *
     * @var string
     */
    protected $html;

    /**
     * Create an instance of GetPropertiesFromHtmlTable class
     *
     * @param string $html The html string
     */
    public function __construct(string $html)
    {
        $this->html = $html;
    }

    /**
     * Handle the command
     *
     * @return array|false
     */
    public function handle()
    {
        $regex = '/(?:<table class=\\\\?"productInfo.+?>)(.+?)(?:<\/table>)/i';

        preg_match_all($regex, $this->html, $tableMatch);

        $full  = array_get($tableMatch, 0);
        $table = array_get($tableMatch, 1);

        if ($table)
        {
            preg_match_all('/<tr>(.+?)<\/tr>/', array_get($table, 0), $rowsMatch);

            $rows = array_get($rowsMatch, 1);
            $cols = [];

            foreach ($rows as $row)
            {
                preg_match_all('/<td>(.+?)<\/td>/', $row, $colsMatch);

                array_set(
                    $cols,
                    array_get($colsMatch, '1.0'),
                    array_get($colsMatch, '1.1')
                );
            }

            return [
                'props' => $cols,
                'full'  => $full,
            ];
        }

        return false;
    }
}
