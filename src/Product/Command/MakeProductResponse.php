<?php namespace Defr\CatalogModule\Product\Command;

use Defr\CatalogModule\Product\ProductLoader;
use Defr\CatalogModule\Product\ProductContent;
use Defr\CatalogModule\Product\ProductResponse;
use Defr\CatalogModule\Product\ProductAuthorizer;
use Defr\CatalogModule\Product\ProductBreadcrumb;
use Defr\CatalogModule\Product\Contract\ProductInterface;

/**
 * Class MakeProductResponse
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class MakeProductResponse
{

    /**
     * The product instance.
     *
     * @var ProductInterface
     */
    private $product;

    /**
     * Create a new MakeProductResponse instance.
     *
     * @param ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }

    /**
     * Handle the command
     *
     * @param ProductLoader     $loader
     * @param ProductContent    $content
     * @param ProductResponse   $response
     * @param ProductAuthorizer $authorizer
     */
    public function handle(
        ProductLoader $loader,
        ProductContent $content,
        ProductResponse $response,
        ProductAuthorizer $authorizer,
        ProductBreadcrumb $breadcrumb
    )
    {
        $authorizer->authorize($this->product);
        $breadcrumb->make($this->product);
        $loader->load($this->product);
        $content->make($this->product);
        $response->make($this->product);
    }
}
