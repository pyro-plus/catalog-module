<?php namespace Defr\CatalogModule\Product\Contract;

use Carbon\Carbon;
use Symfony\Component\HttpFoundation\Response;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;

interface ProductInterface extends EntryInterface
{

    /**
     * Return the product's path.
     *
     * @return string
     */
    public function path();

    /**
     * Get the string ID.
     *
     * @return string
     */
    public function getStrId();

    /**
     * Get the tags.
     *
     * @return array
     */
    public function getTags();

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug();

    /**
     * Get the type.
     *
     * @return null|ProductTypeInterface
     */
    public function getType();

    /**
     * Get the type name.
     *
     * @return string
     */
    public function getTypeName();

    /**
     * Get the type description.
     *
     * @return string
     */
    public function getTypeDescription();

    /**
     * Get the brand.
     *
     * @return null|BrandInterface
     */
    public function getBrand();

    /**
     * Get the related entry.
     *
     * @return EntryInterface
     */
    public function getEntry();

    /**
     * Get the related entry's ID.
     *
     * @return null|int
     */
    public function getEntryId();

    /**
     * Return the publish at date.
     *
     * @return Carbon
     */
    public function getPublishAt();

    /**
     * Return if the product is live or not.
     *
     * @return bool
     */
    public function isLive();

    /**
     * Get the enabled flag.
     *
     * @return bool
     */
    public function isEnabled();

    /**
     * Get the meta title.
     *
     * @return string
     */
    public function getMetaTitle();

    /**
     * Get the meta keywords.
     *
     * @return array
     */
    public function getMetaKeywords();

    /**
     * Get the meta description.
     *
     * @return string
     */
    public function getMetaDescription();

    /**
     * Alias for getPublishAt();
     *
     *
     * @return Carbon
     */
    public function getDate();

    /**
     * Return if the model is
     * restorable or not.
     *
     * @return bool
     */
    public function isRestorable();

    /**
     * Get the path to the product's type layout.
     *
     * @return string
     */
    public function getLayoutViewPath();

    /**
     * Get the content.
     *
     * @return null|string
     */
    public function getContent();

    /**
     * Set the content.
     *
     * @param  $content
     * @return $this
     */
    public function setContent($content);

    /**
     * Get the response.
     *
     * @return Response|null
     */
    public function getResponse();

    /**
     * Set the response.
     *
     * @param  $response
     * @return $this
     */
    public function setResponse(Response $response);

    /**
     * Return the searchable array.
     *
     * @return array
     */
    public function toSearchableArray();

    /**
     * Get featured or not
     *
     * @return bool
     */
    public function getFeatured();

    /**
     * Set and unset featured
     * @param bool $value [description]
     */
    public function setFeatured(bool $value);

    /**
     * Get featuring time
     *
     * @return null|Carbon
     */
    public function getFeaturedAt();

    /**
     * Set time of featuring
     *
     * @param null|Carbon $value
     */
    public function setFeaturedAt($value);

    /**
     * Add product to featured list
     *
     * @param $id [description]
     */
    public function addToFeatured();

    /**
     * Get featured products list
     *
     * @return ProductCollection
     */
    public function getFeaturedProducts();

    /**
     * Return URL of image
     *
     * @return string
     */
    public function getImageUrl();

    /**
     * Return URL of preview image
     *
     * @return string
     */
    public function getPreviewUrl();

    /**
     * Product usage areas relation
     *
     * @return BelongsToMany
     */
    public function productUsages();

    /**
     * Get the product usage areas.
     *
     * @return null|ProductUsageCollection
     */
    public function getProductUsages();
}
