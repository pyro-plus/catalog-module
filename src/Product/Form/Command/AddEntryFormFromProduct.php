<?php namespace Defr\CatalogModule\Product\Form\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\Entry\Form\EntryFormBuilder;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Defr\CatalogModule\Product\Form\ProductEntryFormBuilder;

/**
 * Class AddEntryFormFromProduct
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddEntryFormFromProduct
{

    use DispatchesJobs;

    /**
     * The multiple form builder.
     *
     * @var ProductEntryFormBuilder
     */
    protected $builder;

    /**
     * The Product instance.
     *
     * @var ProductInterface
     */
    protected $product;

    /**
     * Create a new AddEntryFormFromProduct instance.
     *
     * @param ProductEntryFormBuilder $builder
     * @param ProductInterface        $product
     */
    public function __construct(
        ProductEntryFormBuilder $builder,
        ProductInterface $product
    )
    {
        $this->builder = $builder;
        $this->product = $product;
    }

    /**
     * Handle the command.
     *
     * @param EntryFormBuilder $builder
     */
    public function handle(EntryFormBuilder $builder)
    {
        $type = $this->product->getType();

        $builder->setModel($type->getEntryModelName());
        $builder->setEntry($this->product->getEntryId());

        $this->builder->addForm('entry', $builder);
    }
}
