<?php namespace Defr\CatalogModule\Product\Form\Command;

use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\Entry\Form\EntryFormBuilder;
use Defr\CatalogModule\Product\Form\ProductEntryFormBuilder;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;

/**
 * Class AddEntryFormFromRequest
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddEntryFormFromRequest
{

    use DispatchesJobs;

    /**
     * The multiple form builder.
     *
     * @var ProductEntryFormBuilder
     */
    protected $builder;

    /**
     * Create a new AddEntryFormFromRequest instance.
     *
     * @param ProductEntryFormBuilder $builder
     */
    public function __construct(ProductEntryFormBuilder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Handle the command.
     *
     * @param ProductTypeRepositoryInterface $types
     * @param EntryFormBuilder        $builder
     * @param Request                 $request
     */
    public function handle(
        ProductTypeRepositoryInterface $types,
        EntryFormBuilder $builder,
        Request $request
    )
    {
        $type = $types->find($request->get('type'));

        $this->builder->addForm(
            'entry',
            $builder->setModel($type->getEntryModelName())
        );
    }
}
