<?php namespace Defr\CatalogModule\Product\Form\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\Product\Form\ProductFormBuilder;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Defr\CatalogModule\Product\Form\ProductEntryFormBuilder;

/**
 * Class AddProductFormFromProduct
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductFormFromProduct
{

    use DispatchesJobs;

    /**
     * The multiple form builder.
     *
     * @var ProductEntryFormBuilder
     */
    protected $builder;

    /**
     * The Product instance.
     *
     * @var ProductInterface
     */
    protected $product;

    /**
     * Create a new AddProductFormFromProduct instance.
     *
     * @param ProductEntryFormBuilder $builder
     * @param ProductInterface        $product
     */
    public function __construct(
        ProductEntryFormBuilder $builder,
        ProductInterface $product
    )
    {
        $this->builder = $builder;
        $this->product = $product;
    }

    /**
     * Handle the command.
     *
     * @param ProductFormBuilder $builder
     */
    public function handle(ProductFormBuilder $builder)
    {
        $builder->setEntry($this->product->getId());

        $this->builder->addForm('product', $builder);
    }
}
