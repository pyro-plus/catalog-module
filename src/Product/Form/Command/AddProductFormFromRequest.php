<?php namespace Defr\CatalogModule\Product\Form\Command;

use Illuminate\Http\Request;
use Defr\CatalogModule\Product\Form\ProductFormBuilder;
use Defr\CatalogModule\Product\Form\ProductEntryFormBuilder;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;

/**
 * Class AddProductFormFromRequest
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductFormFromRequest
{

    /**
     * The multiple form builder.
     *
     * @var ProductEntryFormBuilder
     */
    protected $builder;

    /**
     * Create a new AddProductFormFromRequest instance.
     *
     * @param ProductEntryFormBuilder $builder
     */
    public function __construct(ProductEntryFormBuilder $builder)
    {
        $this->builder = $builder;
    }

    /**
     * Handle the command.
     *
     * @param ProductTypeRepositoryInterface $types
     * @param ProductFormBuilder      $builder
     * @param Request                 $request
     */
    public function handle(
        ProductTypeRepositoryInterface $types,
        ProductFormBuilder $builder,
        Request $request
    )
    {
        $this->builder->addForm(
            'product',
            $builder->setProductType($types->find($request->get('type')))
        );
    }
}
