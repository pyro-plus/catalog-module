<?php namespace Defr\CatalogModule\Product;

use Anomaly\Streams\Platform\Entry\EntryCollection;
use Defr\CatalogModule\Product\Contract\ProductInterface;

/**
 * Class ProductCollection
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductCollection extends EntryCollection
{

    /**
     * Return only live products.
     *
     * @return ProductCollection
     */
    public function live()
    {
        return $this->filter(
            function (ProductInterface $product)
            {
                return $product->isLive();
            }
        );
    }
}
