<?php namespace Defr\CatalogModule\Product;

use Anomaly\Streams\Platform\Application\Application;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;
use Anomaly\Streams\Platform\Model\Catalog\CatalogProductsEntryModel;
use Carbon\Carbon;
use Defr\CatalogModule\ProductType\Command\GetProductType;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductUsage\ProductUsageModel;
use Defr\CatalogModule\Product\Command\GetProductPath;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Response;

class ProductModel extends CatalogProductsEntryModel implements ProductInterface
{

    /**
     * The products's content.
     *
     * @var null|string
     */
    protected $content = null;

    /**
     * The product's response.
     *
     * @var null|Response
     */
    protected $response = null;

    /**
     * The cache time.
     *
     * @var int
     */
    protected $cacheMinutes = 99999;

    /**
     * The cascaded relations.
     *
     * @var array
     */
    protected $cascades = [
        'entry',
        'variants',
        'properties',
    ];

    /**
     * Eager load these relations.
     *
     * @var array
     */
    protected $with = [
        'entry',
        'translations',
        'variants',
        'properties',
    ];

    /**
     * Restrict to live products only.
     *
     * @param  Builder   $query
     * @return Builder
     */
    public function scopeLive(Builder $query)
    {
        return $query
        ->where('enabled', 1)
        ->where('publish_at', '<=', date('Y-m-d H:i:s'))
        ->orderBy('publish_at', 'DESC');
    }

    /**
     * Return the product's path.
     *
     * @return string
     */
    public function path()
    {
        return $this->dispatch(new GetProductPath($this));
    }

    /**
     * Get the string ID.
     *
     * @return string
     */
    public function getStrId()
    {
        return $this->str_id;
    }

    /**
     * Get the tags.
     *
     * @return array
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get the advantages.
     *
     * @return array
     */
    public function getAdvantages()
    {
        return $this->advantages;
    }

    /**
     * Get the for whom.
     *
     * @return array
     */
    public function getForWhom()
    {
        return $this->for_whom;
    }

    /**
     * Get the advices.
     *
     * @return array
     */
    public function getAdvices()
    {
        return $this->advices;
    }

    /**
     * Get the attentions.
     *
     * @return array
     */
    public function getAttentions()
    {
        return $this->attentions;
    }

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Get the type.
     *
     * @return null|ProductTypeInterface
     */
    public function getType()
    {
        return $this->dispatch(new GetProductType($this->type_id));
    }

    /**
     * Get the type name.
     *
     * @return string
     */
    public function getTypeName()
    {
        $type = $this->getType();

        return $type->getTitle();
    }

    /**
     * Get the type description.
     *
     * @return string
     */
    public function getTypeDescription()
    {
        return $this->getType()->getDescription();
    }

    /**
     * Get the brand.
     *
     * @return null|BrandInterface
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Get the related entry.
     *
     * @return EntryInterface
     */
    public function getEntry()
    {
        return $this->entry;
    }

    /**
     * Get the related entry's ID.
     *
     * @return null|int
     */
    public function getEntryId()
    {
        $entry = $this->getEntry();

        return $entry->getId();
    }

    /**
     * Return the publish at date.
     *
     * @return Carbon
     */
    public function getPublishAt()
    {
        return $this->publish_at;
    }

    /**
     * Return if the product is live or not.
     *
     * @return bool
     */
    public function isLive()
    {
        return $this->isEnabled() &&
        $this->getPublishAt()->diff(new \DateTime())->invert == 0;
    }

    /**
     * Get the enabled flag.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Get the meta title.
     *
     * @return string
     */
    public function getMetaTitle()
    {
        if (!$this->meta_title)
        {
            return $this->getTitle();
        }

        return $this->meta_title;
    }

    /**
     * Get the meta keywords.
     *
     * @return array
     */
    public function getMetaKeywords()
    {
        return $this->meta_keywords;
    }

    /**
     * Get the meta description.
     *
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->meta_description;
    }

    /**
     * Alias for getPublishAt()
     *
     * @return Carbon
     */
    public function getDate()
    {
        return $this->getPublishAt();
    }

    /**
     * Return if the model is
     * restorable or not.
     *
     * @return bool
     */
    public function isRestorable()
    {
        return $this->getType() ? true : false;
    }

    /**
     * Get the path to the product's type layout.
     *
     * @return string
     */
    public function getLayoutViewPath()
    {
        $type = $this->getType();

        /* @var EditorFieldProductType $layout */
        $layout = $type->getFieldType('layout');

        return $layout->getViewPath();
    }

    /**
     * Get the content.
     *
     * @return null|string
     */
    public function getContent()
    {
        return $this->entry->content;
    }

    /**
     * Set the content.
     *
     * @param  $content
     * @return $this
     */
    public function setContent($content)
    {
        $this->entry->content = $content;

        return $this;
    }

    /**
     * Get the response.
     *
     * @return Response|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set the response.
     *
     * @param  $response
     * @return $this
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Return the searchable array.
     *
     * @return array
     */
    public function toSearchableArray()
    {
        $array = parent::toSearchableArray();

        if ($entry = $this->getEntry())
        {
            $array = array_merge($entry->toSearchableArray(), $array);
        }

        return $array;
    }

    /**
     * Get featured or not
     *
     * @return bool
     */
    public function getFeatured()
    {
        return $this->featured_at !== null;
    }

    /**
     * Set and unset featured
     *
     * @param bool $value [description]
     */
    public function setFeatured(bool $value)
    {
        if ($value)
        {
            $this->setFeaturedAt((new Carbon())->now());
        }
        else
        {
            $this->setFeaturedAt(null);
        }

        return $this;
    }

    /**
     * Get featuring time
     *
     * @return null|Carbon
     */
    public function getFeaturedAt()
    {
        return $this->featured_at;
    }

    /**
     * Set time of featuring
     *
     * @param null|Carbon $value
     */
    public function setFeaturedAt($value)
    {
        $this->featured_at = $value;

        return $this;
    }

    /**
     * Add product to featured list
     *
     * @param $id [description]
     */
    public function addToFeatured()
    {
        /* @var ProductCollection */
        $featured = $this->getFeaturedProducts();

        while ($featured->count() > 3)
        {
            $featured->first()->setFeatured(false)->save();
        }

        $this->setFeatured(true);

        return $this;
    }

    /**
     * Get featured products list
     *
     * @return ProductCollection
     */
    public function getFeaturedProducts()
    {
        return $this->newQuery()
        ->where('featured_at', '!=', 'NULL')
        ->orderBy('featured_at', 'DESC')
        ->get();
    }

    /**
     * Gets the image.
     *
     * @return FileInterface
     */
    public function getImage()
    {
        $this->image;
    }

    /**
     * Return URL of image
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->getRelatedFileUrl('image');
    }

    /**
     * Return URL of preview image
     *
     * @return string
     */
    public function getPreviewUrl()
    {
        return $this->getRelatedFileUrl('preview');
    }

    /**
     * Gets the tds.
     *
     * @return <type> The tds.
     */
    public function getTdsUrl()
    {
        return $this->getRelatedFileUrl('tds');
    }

    /**
     * Gets the msds.
     *
     * @return <type> The msds.
     */
    public function getMsdsUrl()
    {
        return $this->getRelatedFileUrl('msds');
    }

    /**
     * Gets the related image url.
     *
     * @param  string $fieldName The field name
     * @return string The related image url.
     */
    protected function getRelatedFileUrl($fieldName)
    {
        $method = camel_case("get_{$fieldName}");

        if (method_exists($this, $method))
        {
            $field = $this->$method();
        }

        $entry = app($this->entry_type)->find($this->entry_id);

        if (method_exists($entry, $method))
        {
            $field = $entry->$method();
        }

        if (!$field = $this->$fieldName)
        {
            $field = $entry->$fieldName;
        }

        if ($field)
        {
            $appReference = app(Application::class)->getReference();
            $diskname     = $field->getDisk()->getSlug();
            $foldername   = $field->getFolder()->getSlug();
            $filename     = $field->getName();

            return url("app/{$appReference}/files-module/{$diskname}/{$foldername}/{$filename}");
        }

        return '';
    }

    /**
     * Product usage areas relation
     *
     * @return BelongsToMany
     */
    public function productUsages()
    {
        return $this->belongsToMany(
            ProductUsageModel::class,
            'catalog_products_product_usages',
            'entry_id',
            'related_id'
        );
    }

    /**
     * Get the product usage areas.
     *
     * @return null|ProductUsageCollection
     */
    public function getProductUsages()
    {
        return $this->productUsages()->get();
    }

    /**
     * Gets the related properties.
     *
     * @return null|PropertyCollection The properties.
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Gets the variants.
     *
     * @return VariantCollection The variants.
     */
    public function getVariants()
    {
        return $this->variants;
    }

    /**
     * Gets the icons.
     *
     * @return <type> The icons.
     */
    public function getIcons()
    {
        return $this->getEntry()->ikonki;
    }
}
