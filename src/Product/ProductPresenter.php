<?php namespace Defr\CatalogModule\Product;

use Anomaly\Streams\Platform\Entry\EntryPresenter;
use Anomaly\Streams\Platform\Support\Decorator;
use Carbon\Carbon;
use Collective\Html\HtmlBuilder;
use Defr\CatalogModule\Product\Contract\ProductInterface;
use Illuminate\Contracts\Config\Repository;

/**
 * Class ProductPresenter
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductPresenter extends EntryPresenter
{

    /**
     * The HTML builder.
     *
     * @var HtmlBuilder
     */
    protected $html;

    /**
     * The decorated product.
     *
     * @var ProductInterface
     */
    protected $object;

    /**
     * The config repository.
     *
     * @var Repository
     */
    private $config;

    /**
     * Create a new ProductPresenter instance.
     *
     * @param HtmlBuilder $html
     * @param Repository  $config
     * @param $object
     */
    public function __construct(HtmlBuilder $html, Repository $config, $object)
    {
        $this->html   = $html;
        $this->config = $config;

        parent::__construct($object);
    }

    /**
     * Get thumb of image
     *
     * @return string
     */
    public function imageLabel()
    {
        $src = str_replace(' ', '%20', $this->object->getImageUrl());

        return '
        <div>
            <img height="50px" src="'.$src.'" />
        </div>';
    }

    /**
     * Return the publish at date.
     *
     * @return Carbon
     */
    public function date()
    {
        return $this->object->getPublishAt();
    }

    /**
     * Fix edit link
     *
     * @return string
     */
    public function editLink()
    {
        return str_replace('/products', '', parent::editLink());
    }

    /**
     * Return the tag links.
     *
     * @param  array    $attributes
     * @return string
     */
    public function tagLinks(array $attributes = [])
    {
        array_set($attributes, 'class', array_get($attributes, 'class', 'label label-default'));

        return array_map(
            function ($label) use ($attributes)
            {
                return $this->html->link(
                    implode(
                        '/',
                        [
                            $this->config->get('defr.module.catalog::paths.module'),
                            $this->config->get('defr.module.catalog::paths.tag'),
                            $label,
                        ]
                    )
                    ,
                    $label,
                    $attributes
                );
            },
            (array) $this->object->getTags()
        );
    }

    /**
     * Return the user's status as a label.
     *
     * @param  string        $size
     * @return null|string
     */
    public function statusLabel($size = 'sm')
    {
        $color  = 'default';
        $status = $this->status();

        switch ($status)
        {
            case 'scheduled':
                $color = 'info';
                break;

            case 'draft':
                $color = 'default';
                break;

            case 'live':
                $color = 'success';
                break;
        }

        return '<span class="label label-'.$size.' label-'.$color.'">'.trans(
            'defr.module.catalog::field.status.option.'.$status
        ).'</span>';
    }

    /**
     * Return the status key.
     *
     * @return null|string
     */
    public function status()
    {
        if (!$this->object->isEnabled())
        {
            return 'draft';
        }

        if ($this->object->isEnabled() && !$this->object->isLive())
        {
            return 'scheduled';
        }

        if ($this->object->isEnabled() && $this->object->isLive())
        {
            return 'live';
        }

        return null;
    }

    public function productUsagesLabel()
    {
        $url   = url()->current();
        $query = url()->getRequest()->all();

        return $this->object->getProductUsages()->map(
            function ($usage) use ($query, $url)
            {
                $queryString = '?';

                foreach ($query as $key => $value)
                {
                    if ($key == 'filter_product_usages')
                    {
                        continue;
                    }

                    $queryString .= "{$key}={$value}&";
                }

                $queryString .= "filter_product_usages={$usage->getId()}";

                return
                    "<a href='{$url}{$queryString}'>
                        {$this->label($usage->getName())}
                    </a>";
            }
        )->implode('');

    }

    /**
     * Catch calls to fields on
     * the page's related entry.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        $entry = $this->object->getEntry();

        if ($entry && $entry->hasField($key))
        {
            return (new Decorator())->decorate($entry)->{$key};
        }

        return parent::__get($key);
    }
}
