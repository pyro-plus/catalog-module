<?php namespace Defr\CatalogModule\Product;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\CatalogModule\Brand\Contract\BrandInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;

class ProductRepository extends EntryRepository implements ProductRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ProductModel
     */
    protected $model;

    /**
     * Create a new ProductRepository instance.
     *
     * @param ProductModel $model
     */
    public function __construct(ProductModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find a product by it's string ID.
     *
     * @param  $id
     * @return null|ProductInterface
     */
    public function findByStrId($id)
    {
        return $this->model->where('str_id', $id)->first();
    }

    /**
     * Find a product by it's slug.
     *
     * @param  $product
     * @return null|ProductInterface
     */
    public function findBySlug($slug)
    {
        return $this->model->orderBy('created_at', 'DESC')->where('slug', $slug)->first();
    }

    /**
     * Find man products by tag.
     *
     * @param  $tag
     * @param  null              $limit
     * @return EntryCollection
     */
    public function findManyByTag($tag, $limit = null)
    {
        return $this->model
        ->live()
        ->where('tags', 'LIKE', '%"'.$tag.'"%')
        ->paginate($limit);
    }

    /**
     * Find many products by category.
     *
     * @param  ProductUsageInterface $category
     * @param  null                  $limit
     * @return EntryCollection
     */
    public function findManyByProductUsage(ProductUsageInterface $category, $limit = null)
    {
        return $this->model
        ->live()
        ->where('category_id', $category->getId())
        ->paginate($limit);
    }

    /**
     * Find many products by brand.
     *
     * @param  BrandInterface    $brand
     * @param  null              $limit
     * @return EntryCollection
     */
    public function findManyByBrand(BrandInterface $brand, $limit = null)
    {
        return $this->model
        ->live()
        ->where('brand_id', $brand->getId())
        ->paginate($limit);
    }

    /**
     * Find many products by type.
     *
     * @param  ProductTypeInterface $type
     * @param  null                 $limit
     * @return ProductCollection
     */
    public function findManyByProductType(ProductTypeInterface $type, $limit = null)
    {
        return $this->model
        ->live()
        ->where('type_id', $type->getId())
        ->paginate($limit);
    }

    /**
     * Find many products by date.
     *
     * @param  $year
     * @param  $month
     * @param  null                $limit
     * @return ProductCollection
     */
    public function findManyByDate($year, $month, $limit = null)
    {
        $query = $this->model
        ->live()
        ->whereYear('publish_at', '=', $year);

        if ($month)
        {
            $query->whereMonth('publish_at', '=', $month);
        }

        return $query->paginate($limit);
    }

    /**
     * Get recent products.
     *
     * @return EntryCollection
     */
    public function getRecent($limit = null)
    {
        return $this->model
        ->live()
        ->paginate($limit);
    }

    /**
     * Get featured products.
     *
     * @return EntryCollection
     */
    public function getFeatured($limit = null)
    {
        return $this->model
        ->live()
        ->where('featured', true)
        ->paginate($limit);
    }

    /**
     * Get live products.
     *
     * @return ProductCollection
     */
    public function getLive()
    {
        return $this->model
        ->live()
        ->get();
    }
}
