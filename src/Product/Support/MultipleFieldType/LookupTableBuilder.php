<?php namespace Defr\CatalogModule\Product\Support\MultipleFieldType;

/**
 * Class LookupTableBuilder
 *
 * @link          http://pyrocms.com/
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 */
class LookupTableBuilder extends \Anomaly\MultipleFieldType\Table\LookupTableBuilder
{

    /**
     * The table columns.
     *
     * @var string
     */
    protected $columns = [
        'title',
        'slug',
    ];

}
