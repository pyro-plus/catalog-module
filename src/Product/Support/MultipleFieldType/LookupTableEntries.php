<?php namespace Defr\CatalogModule\Product\Support\MultipleFieldType;

use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;

/**
 * Class LookupTableEntries
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class LookupTableEntries
{

    /**
     * Product repository
     *
     * @var ProductRepositoryInterface
     */
    protected $products;

    /**
     * Create an instance
     *
     * @param ProductRepositoryInterface $products The products
     */
    public function __construct(ProductRepositoryInterface $products)
    {
        $this->products = $products;
    }

    /**
     * Handle the command
     *
     * @param LookupTableBuilder $builder The builder
     */
    public function handle(LookupTableBuilder $builder)
    {
        $builder->setEntries($this->products->getFeatured());
    }
}
