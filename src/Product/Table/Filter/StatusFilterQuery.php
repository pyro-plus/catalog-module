<?php namespace Defr\CatalogModule\Product\Table\Filter;

use Illuminate\Database\Eloquent\Builder;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Contract\FilterInterface;
use Anomaly\Streams\Platform\Ui\Table\Component\Filter\Query\GenericFilterQuery;

/**
 * Class StatusFilterQuery
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class StatusFilterQuery extends GenericFilterQuery
{

    /**
     * Handle the filter query.
     *
     * @param Builder         $query
     * @param FilterInterface $filter
     */
    public function handle(Builder $query, FilterInterface $filter)
    {
        if ($filter->getValue() == 'live')
        {
            $query->where('enabled', true) ->where('publish_at', '<', time());
        }

        if ($filter->getValue() == 'scheduled')
        {
            $query->where('enabled', true) ->where('publish_at', '>', time());
        }

        if ($filter->getValue() == 'draft')
        {
            $query->where('enabled', false);
        }
    }
}
