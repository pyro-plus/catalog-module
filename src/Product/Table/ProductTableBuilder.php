<?php namespace Defr\CatalogModule\Product\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;
use Defr\CatalogModule\Product\Table\Filter\SkuFilterQuery;
use Defr\CatalogModule\Product\Table\Filter\StatusFilterQuery;

/**
 * Class ProductTableBuilder
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductTableBuilder extends TableBuilder
{

    /**
     * The table views.
     *
     * @var array
     */
    protected $views = [
        'all'      => [
            'columns' => [
                'entry.image_label',
                'entry.edit_link',
                'type',
                'brand',
                'entry.status_label',
            ],
        ],
        'trash'    => [
            'columns' => [
                'title',
                'type',
                'brand',
            ],
        ],
        'featured' => [
            'query'   => 'Defr\CatalogModule\Product\Table\View\FeaturedProductsViewQuery@handle',
            'options' => [
                'sortable' => true,
            ],
            'columns' => [
                'entry.image_label',
                'entry.edit_link',
                'type',
                'brand',
                'entry.status_label',
            ],
        ],
    ];

    /**
     * The table filters.
     *
     * @var array
     */
    protected $filters = [
        'search' => [
            'fields' => [
                'tags',
                'title',
                'meta_title',
                'meta_keywords',
                'meta_description',
            ],
        ],
        'sku'    => [
            'filter'      => 'search',
            'query'       => SkuFilterQuery::class,
            'placeholder' => 'Поиск по артикулу',
        ],
        'type',
        'brand',
        // 'product_usages',
        'status' => [
            'filter'  => 'select',
            'query'   => StatusFilterQuery::class,
            'options' => [
                'live'      => 'defr.module.catalog::field.status.option.live',
                'draft'     => 'defr.module.catalog::field.status.option.draft',
                'scheduled' => 'defr.module.catalog::field.status.option.scheduled',
            ],
        ],
    ];

    /**
     * The tree buttons.
     *
     * @var array
     */
    protected $buttons = [
        'edit',
        'view' => [
            'target' => '_blank',
        ],
    ];

    /**
     * The table actions.
     *
     * @var array
     */
    protected $actions = [
        'delete',
    ];

    /**
     * The table options.
     *
     * @var array
     */
    protected $options = [
        'limit'    => 50,
        'order_by' => [
            'created_at' => 'DESC',
        ],
    ];
}
