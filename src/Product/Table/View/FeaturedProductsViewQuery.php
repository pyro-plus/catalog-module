<?php namespace Defr\CatalogModule\Product\Table\View;

use Defr\CatalogModule\Product\Table\ProductTableBuilder;
use Anomaly\Streams\Platform\Entry\EntryQueryBuilder;

/**
 * Class FeaturedProductsViewQuery
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class FeaturedProductsViewQuery
{

    /**
     * Handle the filter builder.
     *
     * @param TableBuilder $builder
     */
    public function handle(ProductTableBuilder $builder, EntryQueryBuilder $query)
    {
        $query->where('featured', '1');
    }
}
