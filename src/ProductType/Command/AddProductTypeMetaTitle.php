<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Anomaly\Streams\Platform\View\ViewTemplate;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;

/**
 * Class AddProductTypeMetaTitle
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class AddProductTypeMetaTitle
{

    use DispatchesJobs;

    /**
     * The type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create a new AddProductTypeMetaTitle instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param ViewTemplate $template
     */
    public function handle(ViewTemplate $template)
    {
        $template->set('meta_title', $this->type->getName());
    }
}
