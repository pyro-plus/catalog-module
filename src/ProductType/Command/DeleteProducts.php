<?php namespace Defr\CatalogModule\ProductType\Command;

use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;

/**
 * Class DeleteProducts
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class DeleteProducts
{

    /**
     * The product type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create a new DeleteProductTypeStream instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param ProductRepositoryInterface $products
     */
    public function handle(ProductRepositoryInterface $products)
    {
        foreach ($this->type->getProducts() as $product)
        {
            $products->delete($product);
        }
    }
}
