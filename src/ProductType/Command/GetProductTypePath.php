<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Contracts\Config\Repository;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;

/**
 * Class GetProductTypePath
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetProductTypePath
{

    /**
     * The type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create a new GetProductTypePath instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param  Repository $config
     * @return string
     */
    public function handle(Repository $config)
    {
        $module = $config->get('defr.module.catalog::paths.module');
        $type   = $config->get('defr.module.catalog::paths.type');
        $slug   = $this->type->getSlug();

        return "/{$module}/{$type}/{$slug}";
    }
}
