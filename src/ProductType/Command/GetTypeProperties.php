<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\Product\Command\GetPropertiesFromHtmlTable;

/**
 * Class for get type properties.
 */
class GetTypeProperties
{

    use DispatchesJobs;

    /**
     * Products JSON
     *
     * @var string
     */
    protected $productsJson;

    /**
     * Product type
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Create an instance of GetTypeProperties class
     *
     * @param ProductTypeInterface $type     The product type
     * @param string               $jsonPath The json store path
     */
    public function __construct(
        ProductTypeInterface $type,
        string $jsonPath = '/../../../resources/seeders/products.json'
    )
    {
        $this->productsJson = file_get_contents(__DIR__.$jsonPath);
        $this->type         = $type;
    }

    /**
     * Handle the command
     *
     * @return array|false
     */
    public function handle()
    {
        if ($data = json_decode($this->productsJson, true))
        {
            $properties = [];

            $products = array_where($data, function ($product)
            {
                return $this->type->getId() == array_get($product, 'type_id');
            });

            foreach ($products as $product)
            {
                if ($productData = $this->dispatch(
                    new GetPropertiesFromHtmlTable(array_get($product, 'content'))
                ))
                {
                    $properties = array_merge(
                        $properties,
                        array_get($productData, 'props')
                    );
                }
            }

            return $properties;
        }

        return false;
    }
}
