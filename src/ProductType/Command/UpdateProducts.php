<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Defr\CatalogModule\Product\Contract\ProductRepositoryInterface;

/**
 * Class UpdateProducts
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class UpdateProducts
{

    use DispatchesJobs;

    /**
     * The product type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Update a new UpdateProducts instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param ProductTypeRepositoryInterface    $types
     * @param ProductRepositoryInterface $products
     */
    public function handle(
        ProductTypeRepositoryInterface $types,
        ProductRepositoryInterface $products
    )
    {
        /* @var ProductTypeInterface $type */
        $type = $types->find($this->type->getId());

        /* @var ProductInterface $product */
        foreach ($type->getProducts() as $product)
        {
            $products->save(
                $product->setAttribute(
                    'entry_type', $this->type->getEntryModelName()
                )
            );
        }
    }
}
