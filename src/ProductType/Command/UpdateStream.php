<?php namespace Defr\CatalogModule\ProductType\Command;

use Illuminate\Config\Repository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductType\Contract\ProductTypeRepositoryInterface;
use Anomaly\Streams\Platform\Stream\Contract\StreamRepositoryInterface;

/**
 * Class UpdateStream
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class UpdateStream
{

    use DispatchesJobs;

    /**
     * The product type instance.
     *
     * @var ProductTypeInterface
     */
    protected $type;

    /**
     * Update a new UpdateStream instance.
     *
     * @param ProductTypeInterface $type
     */
    public function __construct(ProductTypeInterface $type)
    {
        $this->type = $type;
    }

    /**
     * Handle the command.
     *
     * @param StreamRepositoryInterface $streams
     * @param ProductTypeRepositoryInterface   $types
     * @param Repository                $config
     */
    public function handle(
        StreamRepositoryInterface $streams,
        ProductTypeRepositoryInterface $types,
        Repository $config
    )
    {
        /* @var ProductTypeInterface $type */
        $type = $types->find($this->type->getId());

        /* @var StreamInterface $stream */
        $stream = $type->getEntryStream();

        $stream->fill(
            [
                $config->get('app.fallback_locale') => [
                    'name'        => $this->type->getName(),
                    'description' => $this->type->getDescription(),
                ],
                'slug' => $this->type->getSlug().'_products',
            ]
        );

        $streams->save($stream);
    }
}
