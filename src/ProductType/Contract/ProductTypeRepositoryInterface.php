<?php namespace Defr\CatalogModule\ProductType\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

/**
 * Interface ProductTypeRepositoryInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface ProductTypeRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Get not deleted, not disabled types
     *
     * @return ProductTypeCollection
     */
    public function getLive();

    /**
     * Find a product type by it's slug.
     *
     * @param  $slug
     * @return null|ProductTypeInterface
     */
    public function findBySlug($slug);

    /**
     * Makes an array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection();
}
