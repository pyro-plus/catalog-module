<?php namespace Defr\CatalogModule\ProductType;

use Anomaly\Streams\Platform\Entry\EntryObserver;
use Defr\CatalogModule\ProductType\Command\UpdateStream;
use Defr\CatalogModule\ProductType\Command\DeleteProducts;
use Defr\CatalogModule\ProductType\Command\UpdateProducts;
use Defr\CatalogModule\ProductType\Contract\ProductTypeInterface;
use Defr\CatalogModule\ProductType\Command\CreateProductTypeStream;
use Defr\CatalogModule\ProductType\Command\DeleteProductTypeStream;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Class ProductTypeObserver
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductTypeObserver extends EntryObserver
{

    /**
     * Fired after a page type is created.
     *
     * @param EntryInterface|ProductTypeInterface $entry
     */
    public function created(EntryInterface $entry)
    {
        $this->commands->dispatch(new CreateProductTypeStream($entry));

        parent::created($entry);
    }

    /**
     * Fired before a page type is updated.
     *
     * @param EntryInterface|ProductTypeInterface $entry
     */
    public function updating(EntryInterface $entry)
    {
        $this->commands->dispatch(new UpdateStream($entry));
        $this->commands->dispatch(new UpdateProducts($entry));

        parent::updating($entry);
    }

    /**
     * Fired after a page type is deleted.
     *
     * @param EntryInterface|ProductTypeInterface $entry
     */
    public function deleted(EntryInterface $entry)
    {
        $this->commands->dispatch(new DeleteProducts($entry));
        $this->commands->dispatch(new DeleteProductTypeStream($entry));

        parent::deleted($entry);
    }
}
