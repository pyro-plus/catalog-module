<?php namespace Defr\CatalogModule\ProductType;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class ProductTypePresenter extends EntryPresenter
{

    /**
     * Gets a count of assigned entries label
     *
     * @return string
     */
    public function assignmentsCount()
    {
        return "
        <div class='text-lg-center'>
            <span class='label label-pill label-info'>
                    {$this->object->getEntryStream()->getAssignments()->count()}
            </span>
        </div>
        ";
    }
}
