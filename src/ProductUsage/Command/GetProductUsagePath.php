<?php namespace Defr\CatalogModule\ProductUsage\Command;

use Illuminate\Contracts\Config\Repository;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;

/**
 * Class GetProductUsagePath
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetProductUsagePath
{

    /**
     * The category instance.
     *
     * @var ProductUsageInterface
     */
    protected $category;

    /**
     * Create a new GetProductUsagePath instance.
     *
     * @param ProductUsageInterface $category
     */
    public function __construct(ProductUsageInterface $category)
    {
        $this->category = $category;
    }

    /**
     * Handle the command.
     *
     * @param  Repository $config
     * @return string
     */
    public function handle(Repository $config)
    {
        $category = $config->get('defr.module.catalog::paths.category');
        $module   = $config->get('defr.module.catalog::paths.module');
        $slug     = $this->category->getSlug();

        return "/{$module}/{$category}/{$slug}";
    }
}
