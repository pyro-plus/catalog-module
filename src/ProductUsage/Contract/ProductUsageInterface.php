<?php namespace Defr\CatalogModule\ProductUsage\Contract;

use Anomaly\Streams\Platform\Entry\EntryCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

/**
 * Interface ProductUsageInterface
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
interface ProductUsageInterface extends EntryInterface
{

    /**
     * Gets the identifier.
     *
     * @return integer The identifier.
     */
    public function getId();

    /**
     * Get the name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug();

    /**
     * Gets the video URL.
     *
     * @return string The video.
     */
    public function getVideoUrl();

    /**
     * Gets the image url.
     *
     * @return string The image url.
     */
    public function getImageUrl();

    /**
     * Gets the preview image url.
     *
     * @return strung The preview image url.
     */
    public function getPreviewUrl();

    /**
     * Gets the description.
     *
     * @return string The description.
     */
    public function getDescription();

    /**
     * Gets the meta data.
     *
     * @return array The meta data.
     */
    public function getMetaData();

    /**
     * Return the category's path.
     *
     * @return string
     */
    public function getPath();

    /**
     * Get the related products.
     *
     * @return EntryCollection
     */
    public function getProducts();

    /**
     * Return the products relation.
     *
     * @return HasMany
     */
    public function products();

}
