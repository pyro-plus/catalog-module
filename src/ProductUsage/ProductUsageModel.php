<?php namespace Defr\CatalogModule\ProductUsage;

use Defr\CatalogModule\Product\ProductModel;
use Anomaly\Streams\Platform\Entry\EntryCollection;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Defr\CatalogModule\ProductUsage\Command\GetProductUsagePath;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;
use Anomaly\Streams\Platform\Model\Catalog\CatalogProductUsagesEntryModel;

/**
 * Class ProductUsageModel
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductUsageModel extends CatalogProductUsagesEntryModel implements ProductUsageInterface
{

    /**
     * The cache time.
     *
     * @var int
     */
    protected $cacheMinutes = 99999;

    /**
     * Gets the identifier.
     *
     * @return integer The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get the slug.
     *
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Gets the video URL.
     *
     * @return string The video.
     */
    public function getVideoUrl()
    {
        return $this->video;
    }

    /**
     * Gets the image url.
     *
     * @return string The image url.
     */
    public function getImageUrl()
    {
        return $this->image ? $this->image->url() : '';
    }

    /**
     * Gets the preview image url.
     *
     * @return strung The preview image url.
     */
    public function getPreviewUrl()
    {
        return $this->preview ? $this->preview->url() : '';
    }

    /**
     * Gets the description.
     *
     * @return string The description.
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Gets the meta data.
     *
     * @return array The meta data.
     */
    public function getMetaData()
    {
        return [
            'title'       => $this->meta_title,
            'description' => $this->meta_description,
            'keywords'    => $this->meta_keywords,
        ];
    }

    /**
     * Return the category's path.
     *
     * @return string
     */
    public function getPath()
    {
        return $this->dispatch(new GetProductUsagePath($this));
    }

    /**
     * Get the related products.
     *
     * @return EntryCollection
     */
    public function getProducts()
    {
        return $this->products()->get();
    }

    /**
     * Return the products relation.
     *
     * @return BelongsToMany
     */
    public function products()
    {
        return $this->hasMany(ProductModel::class);
    }
}
