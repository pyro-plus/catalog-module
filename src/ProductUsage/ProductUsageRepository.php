<?php namespace Defr\CatalogModule\ProductUsage;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageInterface;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageRepositoryInterface;

/**
 * Class ProductUsageRepository
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class ProductUsageRepository extends EntryRepository implements ProductUsageRepositoryInterface
{

    /**
     * The category model.
     *
     * @var ProductUsageModel
     */
    protected $model;

    /**
     * Create a new ProductUsageRepository instance.
     *
     * @param ProductUsageModel $model
     */
    public function __construct(ProductUsageModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find a category by it's related
     * products and it's slug.
     *
     * @param  $slug
     * @return null|ProductUsageInterface
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
     * Makes an array for vue-app.
     *
     * @return Collection
     */
    public function toVueCollection()
    {
        /* @var ProductUsageInterface $category */
        return $this->model->all()->map(
            function (ProductUsageInterface $category)
            {
                return [
                    'id'          => $category->getId(),
                    'name'        => $category->getName(),
                    'slug'        => $category->getSlug(),
                    'meta'        => $category->getMetaData(),
                    'video'       => $category->getVideoUrl(),
                    'image_url'   => $category->getImageUrl(),
                    'preview_url' => $category->getPreviewUrl(),
                    'description' => $category->getDescription(),
                ];
            }
        );
    }
}
