<?php namespace Defr\CatalogModule\ProductUsage;

use Illuminate\Contracts\Config\Repository;
use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\CatalogModule\ProductUsage\Contract\ProductUsageRepositoryInterface;

/**
 * Class ProductUsageSeeder
 *
 * @author PyroCMS, Inc. <support@pyrocms.com>
 * @author Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link   http://pyrocms.com/
 */
class ProductUsageSeeder extends Seeder
{

    /**
     * The category repository.
     *
     * @var ProductUsageRepositoryInterface
     */
    protected $product_usages;

    /**
     * The config repository.
     *
     * @var Repository
     */
    protected $config;

    /**
     * Create a new ProductUsageSeeder instance.
     *
     * @param Repository                      $config
     * @param ProductUsageRepositoryInterface $product_usages
     */
    public function __construct(
        Repository $config,
        ProductUsageRepositoryInterface $product_usages
    )
    {
        $this->config         = $config;
        $this->product_usages = $product_usages;
    }

    /**
     * Run the seeder.
     */
    public function run()
    {
        echo "\n\n\033[34;5;228mStarting product_usages seeder!\n";

        $this->product_usages->truncate();

        echo "\033[35;5;228mProductUsages cleared!\n";

        if ($data = file_get_contents(__DIR__.'/../../resources/seeders/product_usages.json'))
        {
            $trans = json_decode(file_get_contents(__DIR__.'/../../resources/seeders/product_usages_trans.json'), true);

            foreach (json_decode($data, true) as $usage)
            {
                // $usageProperties  = [];
                // $usageName        = array_get($usage, 'name');
                // $usageDescription = array_get($usage, 'description');
                $name = '';

                foreach (array_filter($trans, function ($transEntry) use ($usage, $name)
                {
                    $name = array_get($transEntry, 'name', '');
                    return array_get($transEntry, 'entry_id') === array_get($usage, 'id');
                }) as $translation)
                {
                    array_set($usage, array_get($translation, 'locale'), $translation);
                }

                /* @var ProductUsageInterface $usage */
                $usage = $this->product_usages->create($usage);

                echo "\033[36;5;228mCreated category \033[31;5;228m".$name."\n";
            }

            echo "\033[32;5;228mProductUsages seeded successfully!\n";
        }
    }
}
