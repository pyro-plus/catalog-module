<?php namespace Defr\CatalogModule\Property;

use Defr\CatalogModule\Property\Contract\PropertyRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class PropertyRepository extends EntryRepository implements PropertyRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var PropertyModel
     */
    protected $model;

    /**
     * Create a new PropertyRepository instance.
     *
     * @param PropertyModel $model
     */
    public function __construct(PropertyModel $model)
    {
        $this->model = $model;
    }
}
