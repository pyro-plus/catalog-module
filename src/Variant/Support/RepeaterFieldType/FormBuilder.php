<?php namespace Defr\CatalogModule\Variant\Support\RepeaterFieldType;

class FormBuilder extends \Anomaly\Streams\Platform\Ui\Form\FormBuilder
{

    /**
     * The form fields.
     *
     * @var array|string
     */
    protected $fields = [
        'sku',
        'color',
        'capacity',
        'value' => [
            'label' => 'Форма выпуска',
        ],
    ];

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [
        'price',
    ];
}
