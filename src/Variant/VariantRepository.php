<?php namespace Defr\CatalogModule\Variant;

use Defr\CatalogModule\Variant\Contract\VariantRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class VariantRepository extends EntryRepository implements VariantRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var VariantModel
     */
    protected $model;

    /**
     * Create a new VariantRepository instance.
     *
     * @param VariantModel $model
     */
    public function __construct(VariantModel $model)
    {
        $this->model = $model;
    }
}
